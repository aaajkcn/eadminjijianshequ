<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\logic\Common as LogicCommon;


class Comment extends  HomeBase
{
	private static $commonLogic = null;
	public function _initialize()
	{
		parent::_initialize();
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
	}
	
   public function addcomment(){
   	
   $data=$this->param;
   $data['uid']=session('member_info')['id'];

   $where['uid']=$data['uid'];
   $where['fid']=$data['fid'];
   
   if(model('comment')->where($where)->count()>0){
   	$this->jump([RESULT_ERROR, '已对该文档进行过评论']);
   }else{
   	$this->jump(self::$commonLogic->dataAdd('comment',$data,true,'添加评论成功'));
   }
   
   	
   	
   }
}
