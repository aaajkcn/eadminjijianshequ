<?php
namespace addon\alipay;

use app\common\controller\AddonBase;

use addon\AddonInterface;


class Alipay extends AddonBase implements AddonInterface
{
    private $error;

    public $info = array(
        'name' => 'Alipay',
        'title' => '支付宝即时到账',
        'description' => '用于积分充值',
        'status' => 1,
        'author' => 'eadmin',
        'version' => '0.1'
    );

    public $admin_list = array(
        'model' => 'dingdan', //要查的表
        'fields' => '*', //要查的字段
        'map' => '', //查询条件, 如果需要可以再插件类的构造方法里动态重置这个属性
        'order' => 'status desc,add_time desc', //排序,
        'listKey' => array( //这里定义的是除了id序号外的表格里字段显示的表头名
            'id' => '订单ID',
        		'trade_no'=> '支付宝订单',
        		'uid' => '用户ID',	
            'jiage' => '金额',
            'score' => '积分',
            'status' => '状态',
            'errorcode' => '错误代码',
            'create_time' => '订单时间',
        ),
    );
    public $custom_adminlist = 'adminlist.html';
    /**
     * 插件安装
     */
    public function addonInstall()
    {
    	$arr=$this->addonInfo();
    	$this->getisHook('showpay', $arr['name'], $arr['describe']);
    	
    	$this->installAddon($arr);
    	 
    	//$this->addonCacheUpdate();
    
    	return [RESULT_SUCCESS, '安装成功'];
    }
    
    /**
     * 插件卸载
     */
    public function addonUninstall()
    {
    	$arr=$this->addonInfo();
    	$this->deleteHook('showpay');
    	$this->uninstallAddon($arr['name']);
    	//$this->addonCacheUpdate();
    
    	return [RESULT_SUCCESS, '卸载成功'];
    }
    
    /**
     * 插件基本信息
     */
    public function addonInfo()
    {
    
    	return [
    	'name' => 'alipay',
        'title' => '支付宝即时到账',
        'describe' => '用于积分充值',
        'author' => 'zaker',
        'version' => '0.1',
    	'has_adminlist' => '1'
    	];
    }
    

   
   
    public function showpay()
    {
    	
    	$config = $this->getConfig('alipay');
    	
    	$this->assign('config',$config);
    	echo  $this->tplfetch('showpay');
    }


   
  

}